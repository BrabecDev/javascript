
		var startTime = Date.now();

		function shape(){
			var shapes = ["circle", "square"];
			var chosen = Math.floor(Math.random() * 2);

			switch(shapes[chosen]){
				case "circle":
					return "50%";
				case "square":
					return "0%";
				default:
					break;
			}
		}

		//returns an array with top and left values
		function getPosition(){

			var top = Math.floor(Math.random() * 300).toString();
			var left = Math.floor(Math.random() * 800).toString();

			var position = [top, left];

			return position;
		}

		//OK
		function getColor(){
			var colors = ["black", "blue", "gray", "green", "yellow", "pink"];
			var chosenColor = Math.floor(Math.random() * colors.length);

			return colors[chosenColor];
		}

		//Perfect!
		function getRandomColor() {
                
            var letters = '0123456789ABCDEF'.split('');

            var color = '#';

            for (var i = 0; i < 6; i++ ) {
    
                color += letters[Math.floor(Math.random() * 16)];

            }

            return color;

        }

		//return a size between 100-600px
		function size(){
			var size = Math.floor((Math.random() * 300) + 50);

			return size + "px";
		}

		function timer() {
			startTime = Date.now();

			var interval = setInterval(function() {
			    var elapsedTime = Date.now() - startTime; // It's in ms
			    document.getElementById("timer").innerHTML = (elapsedTime / 1000).toFixed(3) + "s";
			}, 100);
		}

		function start(){

			var dimension = size();
			var color = getRandomColor();
			var borderRadius = shape();

			var position = getPosition();
			var top = position[0] + "px";
			var left = position[1] + "px";

			document.getElementById('box').style.display = "block";
			document.getElementById('timer').style.display = "inline";
			document.getElementById('box').style.height = dimension;
			document.getElementById('box').style.width = dimension;
			document.getElementById('box').style.borderRadius = borderRadius;
			document.getElementById('box').style.backgroundColor = color;
			document.getElementById('box').style.top = top;
			document.getElementById('box').style.left = left;

			timer();

		}
		
		function disappear() {
			document.getElementById('box').style.display = "none";
			document.getElementById('timer').style.display = "none";
		}

		document.getElementById("box").onclick = function() {
			var time = Math.random() * 2000;
			disappear();
			
			setTimeout(start, time);


		};

		start();